package person.zt.spring_boot_starter_hello;

/**
 * Created by zt on 16/12/23.
 */
public class HelloService {

    private String msg;

    public String sayHello(){
        return "Hello " + msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
